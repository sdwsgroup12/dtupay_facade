# @author Cesare s232435
Feature: Manager Facade

  Scenario: Successful manager report generation
    Given there is a payment registered in DTUPay
    When a manager requests a report
    Then an event "ManagerReportRequested" should be published
    When an event "ManagerReportGenerated" event is received
    Then the manager should receive a report
