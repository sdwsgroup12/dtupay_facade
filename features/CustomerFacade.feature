# @author Can s232851
Feature: Customer Facade

  Scenario: Customer registration
    Given there is a customer with empty id
    When the customer is being registered
    Then the "CustomerRegistrationRequested" event should be published
    When the "CustomerRegistered" event is handled
    Then the customer should have a DTUPay id

  Scenario: Customer deregistration
    Given there is a customer with a DTUPay id
    When the customer is being deregistered
    Then the "CustomerDeregistrationRequested" event should be published
    When the "CustomerDeregistered" event is handled
    Then the customer should have no DTUPay id

  Scenario: Tokens are obtained successfully
    Given there is a customer with a DTUPay id
    When the customer requests a token
    Then the "TokensGenerationRequested" event should be published
    When the "TokensGenerated" event is handled
    Then the customer should have a list of one token

  Scenario: Report is generated successfully
    Given there is a customer with a DTUPay id
    And a registered payment
    When the customer requests a report
    Then the "CustomerReportRequested" event should be published
    When the "CustomerReportGenerated" event is handled
    Then the customer should have a report

  Scenario: Report generation fails
    Given there is a customer not registered in DTUPay
    When the customer requests a report
    Then the "CustomerReportRequested" event should be published
    When the "CustomerReportGenerationFailed" event is handled
    Then an exception with message "Customer not found" should be thrown

  Scenario: Token Generation fails
    Given there is a customer with a DTUPay id
    When the customer requests a token
    Then the "TokensGenerationRequested" event should be published
    When the "TokensGenerationFailed" event is handled
    Then an exception with message "Customer already has available tokens" should be thrown
