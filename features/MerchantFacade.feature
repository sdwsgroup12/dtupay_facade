# @author Ion s204710
Feature: Merchant facade

  Scenario: Successful Merchant registration
    Given there is a merchant with empty id
    When the merchant is being registered
    Then the "MerchantRegistrationRequested" event needs to be published
    When the "MerchantRegistered" event is being handled
    Then the merchant should have a DTUPay id
