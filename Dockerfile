FROM eclipse-temurin:21 as jre-build
COPY /target/quarkus-app /usr/src/
WORKDIR /usr/src/
CMD java -Xmx32m -jar quarkus-run.jar
