package behaviourtests;

import dtuPay.facade.ManagerFacade;
import dtuPay.facade.model.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.jupiter.api.Assertions;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

// @author: Matteo s222952
public class ManagerFacadeSteps {
    Payment payment;
    CompletableFuture<Report> reportGenerated = new CompletableFuture<>();
    CompletableFuture<Event> reportEventGenerated = new CompletableFuture<>();
    CorrelationId correlationId;
    private MessageQueue q = new MessageQueue() {

        @Override
        public void publish(Event event) {
            String customerId;
            switch (event.getType()) {
                case "ManagerReportRequested":
                    reportEventGenerated.complete(event);
                    break;
            }

        }

        @Override
        public void addHandler(String eventType, Consumer<Event> handler) {
        }

    };
    ManagerFacade managerFacade = new ManagerFacade(q);

    @Given("there is a payment registered in DTUPay")
    public void thereIsAPaymentRegisteredInDTUPay() {
        payment = new Payment();
        payment.setPaymentID("123");
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(100);
        paymentRequest.setMerchantID("123");
        payment.setPaymentRequest(paymentRequest);
        payment.setCustomerID("345");

    }

    @When("a manager requests a report")
    public void aManagerRequestsAReport() {
        new Thread(() -> {
            Report result = null;
            try {
                result = managerFacade.getReport();
            } catch (Exception e) {
                Assertions.fail(e.getMessage());
            }
            reportGenerated.complete(result);
        }).start();
    }

    @Then("an event {string} should be published")
    public void anEventShouldBePublished(String eventName) {
        switch (eventName) {
            case "ManagerReportRequested":
                Event event = reportEventGenerated.join();
                correlationId = event.getArgument(0, CorrelationId.class);
                Assertions.assertEquals(eventName, event.getType());
                break;
        }
    }

    @When("an event {string} event is received")
    public void anEventEventIsReceived(String eventName) {
        switch (eventName) {
            case "ManagerReportGenerated":
                Report report = new Report();
                ReportRequest reportRequest = new ReportRequest();
                report.setReportRequest(reportRequest);
                report.setPayments(List.of(payment));
                managerFacade.handleManagerReportGenerated(new Event(eventName, new Object[]{report, correlationId}));
        }
    }


    @Then("the manager should receive a report")
    public void theManagerShouldReceiveAReport() {
        Report report = reportGenerated.join();
        Assertions.assertEquals(payment, report.getPayments().get(0));
    }
}
