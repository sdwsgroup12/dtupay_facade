package behaviourtests;

import dtuPay.facade.MerchantFacade;
import dtuPay.facade.adapter.rest.MerchantsServiceException;
import dtuPay.facade.model.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static io.smallrye.common.constraint.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;


// @author Alexander s204092
public class MerchantFacadeSteps {
    Payment payment;
    Transaction transaction;
    CompletableFuture<Transaction> transactionsGenerated = new CompletableFuture<>();
    private Map<String, CompletableFuture<Event>> publishedEvents = new HashMap<>();
    private CompletableFuture<Report> reportGenerated = new CompletableFuture<>();
    private MessageQueue q = new MessageQueue() {
        @Override
        public void publish(Event event) {
            String merchantId;
            switch (event.getType()) {
                case "MerchantDeregistrationRequested":
                    merchantId = event.getArgument(0, String.class);
                    publishedEvents.get(merchantId).complete(event);
                    return;
                case "MerchantRegistrationRequested":
                    var merchant = event.getArgument(0, Client.class);
                    publishedEvents.get(merchant.getId()).complete(event);
                    break;
                case "TokenNotValidated":
                    var paymentRequest = event.getArgument(0, Client.class);
                    publishedEvents.get(paymentRequest).complete(event);
                    break;
                default:
                    break;
            }
        }

        @Override
        public void addHandler(String eventType, Consumer<Event> handler) {
        }

    };
    private MerchantFacade merchantService = new MerchantFacade(q);
    private CompletableFuture<String> registeredMerchantId = new CompletableFuture<>();
    private CompletableFuture<ArrayList<Token>> tokenGenerated = new CompletableFuture<>();
    private Client merchant;
    private Client customer;
    private Map<String, CorrelationId> correlationIds = new HashMap<>();
    private PaymentRequest paymentRequest;

    @Then("the merchant should have a DTUPay id")
    public void theMerchantShouldHaveADTUPayId() {
        String id = registeredMerchantId.join();
        assertNotNull(id);
        Assertions.assertEquals(id, merchant.getId());
    }

    @Given("there is a merchant with empty id")
    public void thereIsAMerchantWithEmptyId() {
        merchant = new Client();
        merchant.setName("James");
        merchant.setCpr("1234567890");
        merchant.setAccountId("1234567890");
        assertNull(merchant.getId());
        publishedEvents.put(merchant.getId(), new CompletableFuture<Event>());
    }

    @When("the merchant is being registered")
    public void theMerchantIsBeingRegistered() {
        new Thread(() -> {
            String result = null;
            result = merchantService.register(merchant);
            merchant.setId(result);
            registeredMerchantId.complete(result);
        }).start();
    }


    @Given("there is a merchant with a DTUPay id")
    public void thereIsAMerchantWithADTUPayId() {
        merchant = new Client();
        merchant.setName("James");
        merchant.setCpr("1234567890");
        merchant.setAccountId("1234567890");
        merchant.setId("123");
        publishedEvents.put(merchant.getId(), new CompletableFuture<Event>());
    }

    @When("the merchant is being deregistered")
    public void theMerchantIsBeingDeregistered() {
        new Thread(() -> {
            merchantService.deregister(merchant.getId());
        }).start();
    }

    @Then("the merchant should have no DTUPay id")
    public void theMerchantShouldHaveNoDTUPayId() {
        assertNull(merchant.getId());
    }


    @Then("throw {string} exception")
    public void throw_exception(String event) {

    }


    @When("the merchant requests a transaction")
    public void theMerchantRequestsATransaction() {
        new Thread(() -> {
            try {
                transactionsGenerated.complete(merchantService.pay(paymentRequest));
            } catch (MerchantsServiceException e) {
                Assertions.fail(e.getMessage());
            }
        }).start();
    }

    @Then("the {string} event needs to be published")
    public void theEventNeedsToBePublished(String eventName) {
        Event event;
        CorrelationId correlationId;
        switch (eventName) {
            case "MerchantRegistrationRequested":
                event = publishedEvents.get(merchant.getId()).join();
                Assertions.assertEquals(eventName, event.getType());
                var merchant = event.getArgument(0, Client.class);
                correlationId = event.getArgument(1, CorrelationId.class);
                Assertions.assertEquals(merchant.getAccountId(), this.merchant.getAccountId());
                correlationIds.put(merchant.getAccountId(), correlationId);
                break;
            case "MerchantDeregistrationRequested":
                event = publishedEvents.get(this.merchant.getId()).join();
                var clientId = event.getArgument(0, String.class);
                correlationId = event.getArgument(1, CorrelationId.class);
                Assertions.assertEquals(clientId, this.merchant.getId());
                correlationIds.put(clientId, correlationId);
                Assertions.assertEquals(eventName, event.getType());
                break;
            case "TransactionRequested":
                event = publishedEvents.get(this.merchant.getId()).join();
                var requestedPayment = event.getArgument(0, PaymentRequest.class);
                correlationId = event.getArgument(1, CorrelationId.class);
                Assertions.assertEquals(requestedPayment.getMerchantID(), this.merchant.getId());
                correlationIds.put(this.merchant.getId(), correlationId);
                Assertions.assertEquals(eventName, event.getType());
                break;
            case "MerchantReportRequested":
                event = publishedEvents.get(this.merchant.getId()).join();
                var reportRequest = event.getArgument(0, ReportRequest.class);
                correlationId = event.getArgument(1, CorrelationId.class);
                correlationIds.put(reportRequest.getClientId(), correlationId);
                Assertions.assertEquals(reportRequest.getClientId(), this.merchant.getId());
                Assertions.assertEquals(eventName, event.getType());
                break;

            default:
                break;
        }

    }

    @When("the {string} event is being handled")
    public void theEventIsBeingHandled(String eventName) {
        switch (eventName) {
            case "MerchantRegistered":
                var m = new Client();
                m.setName(merchant.getName());
                m.setCpr(merchant.getCpr());
                m.setAccountId(merchant.getAccountId());
                m.setId("123");
                merchantService.handleMerchantRegistered(new Event(eventName, new Object[]{m, correlationIds.get(m.getAccountId())}));
                break;
            case "MerchantDeregistered":
                merchantService.handleMerchantDeRegistered(new Event(eventName, new Object[]{merchant, correlationIds.get(merchant.getId())}));
                merchant.setId(null);
                break;
            case "TransactionCompleted":
                merchantService.handleTransactionCompleted(new Event(eventName, new Object[]{transaction, correlationIds.get(transaction.getPayment().getPaymentRequest().getMerchantID())}));
                break;
            case "MerchantReportGenerated":
                Report report = new Report();
                ReportRequest reportRequest = new ReportRequest(merchant.getId());
                report.setReportRequest(reportRequest);
                report.setPayments(List.of(payment));

                merchantService.handleMerchantReportGenerated(new Event(eventName, new Object[]{report, correlationIds.get(merchant.getId())}));
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + eventName);
        }
    }

    @Then("an exception with message {string} should be received")
    public void anExceptionWithMessageShouldBeReceived(String arg0) {
    }

    @And("and a customer registered with DTUPay")
    public void andACustomerRegisteredWithDTUPay() {
        customer = new Client();
        customer.setName("James");
        customer.setAccountId("999");
        customer.setCpr("1234567890");
        customer.setId("999");

        transaction = new Transaction();
        Payment payment = new Payment();
        paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(100);
        paymentRequest.setMerchantID(merchant.getId());
        payment.setPaymentRequest(paymentRequest);
        payment.setPaymentID("222");
        payment.setCustomerID(customer.getId());
        transaction.setPayment(payment);
    }

    @Then("the merchant receives the correct payment")
    public void theMerchantReceivesTheCorrectPayment() {
    }

    @Then("the merchant should have a report")
    public void theMerchantShouldHaveAReport() {
        Report report = reportGenerated.join();
        Assertions.assertEquals(1, report.getPayments().size());
        Assertions.assertEquals(payment, report.getPayments().get(0));
    }

    @And("and a payment has been registered")
    public void andAPaymentHasBeenRegistered() {
        payment = new Payment();
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(100);
        paymentRequest.setMerchantID("123");
        paymentRequest.setToken(new Token());
        payment.setPaymentRequest(paymentRequest);
        payment.setPaymentID("111");
        payment.setCustomerID(customer.getId());
    }
}
