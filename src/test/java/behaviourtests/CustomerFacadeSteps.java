package behaviourtests;

import dtuPay.facade.CustomerFacade;
import dtuPay.facade.adapter.rest.CustomersServiceException;
import dtuPay.facade.model.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static io.smallrye.common.constraint.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

// @author Ion s204710
public class CustomerFacadeSteps {
    Payment payment;
    private Map<String, CompletableFuture<Event>> publishedEvents = new HashMap<>();
    private MessageQueue q = new MessageQueue() {

        @Override
        public void publish(Event event) {
            String customerId;
            switch (event.getType()) {
                case "CustomerDeregistrationRequested":
                    customerId = event.getArgument(0, String.class);
                    publishedEvents.get(customerId).complete(event);
                    return;
                case "CustomerRegistrationRequested":
                    var customer = event.getArgument(0, Client.class);
                    publishedEvents.get(customer.getId()).complete(event);
                    break;
                case "TokensGenerationRequested":
                    var tokenRequest = event.getArgument(0, TokenRequest.class);
                    customerId = tokenRequest.getCustomerId();
                    publishedEvents.get(customerId).complete(event);
                    break;
                case "CustomerReportRequested":
                    var reportRequest = event.getArgument(0, ReportRequest.class);
                    customerId = reportRequest.getClientId();
                    publishedEvents.get(customerId).complete(event);
                    break;
                default:
                    break;

            }

        }

        @Override
        public void addHandler(String eventType, Consumer<Event> handler) {
        }

    };
    private CustomerFacade customerService = new CustomerFacade(q);
    private CompletableFuture<String> registeredCustomerId = new CompletableFuture<>();
    private CompletableFuture<ArrayList<Token>> tokenGenerated = new CompletableFuture<>();
    private CompletableFuture<Report> reportGenerated = new CompletableFuture<>();
    private CompletableFuture<String> exceptionThrown = new CompletableFuture<>();
    private Client customer;
    private Map<String, CorrelationId> correlationIds = new HashMap<>();

    @Then("the customer should have a DTUPay id")
    public void theCustomerShouldHaveADTUPayId() {
        String id = registeredCustomerId.join();
        assertNotNull(id);
        Assertions.assertEquals(id, customer.getId());
//        customer.setId(id);

    }

    @Given("there is a customer with empty id")
    public void thereIsACustomerWithEmptyId() {
        customer = new Client();
        customer.setName("James");
        customer.setCpr("1234567890");
        customer.setAccountId("1234567890");
        assertNull(customer.getId());
        publishedEvents.put(customer.getId(), new CompletableFuture<Event>());
    }

    @When("the customer is being registered")
    public void theCustomerIsBeingRegistered() {
        new Thread(() -> {
            String result = null;
            result = customerService.register(customer);

            customer.setId(result);
            registeredCustomerId.complete(result);
        }).start();
    }

    @Then("the {string} event should be published")
    public void theEventShouldBePublished(String eventName) {
        Event event;
        CorrelationId correlationId;
        switch (eventName) {
            case "CustomerRegistrationRequested":
                event = publishedEvents.get(customer.getId()).join();
                Assertions.assertEquals(eventName, event.getType());
                var customer = event.getArgument(0, Client.class);
                correlationId = event.getArgument(1, CorrelationId.class);
                Assertions.assertEquals(customer.getAccountId(), this.customer.getAccountId());
                correlationIds.put(customer.getAccountId(), correlationId);
                break;
            case "CustomerDeregistrationRequested":
                event = publishedEvents.get(this.customer.getId()).join();
                var clientId = event.getArgument(0, String.class);
                correlationId = event.getArgument(1, CorrelationId.class);
                Assertions.assertEquals(clientId, this.customer.getId());
                correlationIds.put(clientId, correlationId);
                Assertions.assertEquals(eventName, event.getType());
                break;
            case "TokensGenerationRequested":
                event = publishedEvents.get(this.customer.getId()).join();
                var tokenRequest = event.getArgument(0, TokenRequest.class);
                correlationId = event.getArgument(1, CorrelationId.class);
                Assertions.assertEquals(tokenRequest.getCustomerId(), this.customer.getId());
                Assertions.assertEquals(1, tokenRequest.getAmount());
                correlationIds.put(tokenRequest.getCustomerId(), correlationId);
                Assertions.assertEquals(eventName, event.getType());
                break;
            case "CustomerReportRequested":
                event = publishedEvents.get(this.customer.getId()).join();
                var reportRequest = event.getArgument(0, ReportRequest.class);
                correlationId = event.getArgument(1, CorrelationId.class);
                correlationIds.put(reportRequest.getClientId(), correlationId);
                Assertions.assertEquals(reportRequest.getClientId(), this.customer.getId());
                Assertions.assertEquals(eventName, event.getType());
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + eventName);
        }

    }

    @When("the {string} event is handled")
    public void theEventIsHandled(String eventName) {
        switch (eventName) {
            case "CustomerRegistered":
                var c = new Client();
                c.setName(customer.getName());
                c.setCpr(customer.getCpr());
                c.setAccountId(customer.getAccountId());
                c.setId("123");
                customerService.handleCustomerRegistered(new Event(eventName, new Object[]{c, correlationIds.get(c.getAccountId())}));
                break;
            case "CustomerDeregistered":
                customerService.handleCustomerDeRegistered(new Event(eventName, new Object[]{customer.getId(), correlationIds.get(customer.getId())}));
                customer.setId(null);
                break;
            case "TokensGenerated":
                Token token = new Token();
                token.setTokenID("123");
                customerService.handleTokensGenerated(new Event(eventName, new Object[]{List.of(token), correlationIds.get(customer.getId())}));
                break;
            case "CustomerReportGenerated":
                Report report = new Report();
                ReportRequest reportRequest = new ReportRequest(customer.getId());
                report.setReportRequest(reportRequest);
                report.setPayments(List.of(payment));

                customerService.handleCustomerReportGenerated(new Event(eventName, new Object[]{report, correlationIds.get(customer.getId())}));
                break;
            case "CustomerReportGenerationFailed":
                customerService.handleCustomerReportNotGenerated(new Event(eventName, new Object[]{"Customer not found", correlationIds.get(customer.getId())}));
                break;
            case "TokensGenerationFailed":
                customerService.handleTokenGenerationFailed(new Event(eventName, new Object[]{"Customer already has available tokens", correlationIds.get(customer.getId())}));
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + eventName);
        }
    }

    @Given("there is a customer with a DTUPay id")
    public void thereIsACustomerWithADTUPayId() {
        customer = new Client();
        customer.setName("James");
        customer.setCpr("1234567890");
        customer.setAccountId("1234567890");
        customer.setId("123");
        publishedEvents.put(customer.getId(), new CompletableFuture<Event>());
    }

    @When("the customer is being deregistered")
    public void theCustomerIsBeingDeregistered() {
        new Thread(() -> {
            customerService.deregister(customer.getId());
        }).start();
    }

    @Then("the customer should have no DTUPay id")
    public void theCustomerShouldHaveNoDTUPayId() {
        assertNull(customer.getId());
    }

    @When("the customer requests a token")
    public void theCustomerRequestsAToken() {
        new Thread(() -> {
            TokenRequest request = new TokenRequest();
            request.setCustomerId(customer.getId());
            request.setAmount(1);
            try {
                ArrayList<Token> tokens = customerService.getTokens(request);
                tokenGenerated.complete(tokens);
            } catch (Exception e) {
                exceptionThrown.complete(e.getMessage());
            }
        }).start();
    }

    @Then("the customer should have a list of one token")
    public void theCustomerShouldHaveAListOfOneToken() {
        ArrayList<Token> tokens = tokenGenerated.join();
        Assertions.assertEquals(1, tokens.size());
    }

    @And("a registered payment")
    public void aRegisteredPayment() {
        payment = new Payment();
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(100);
        paymentRequest.setMerchantID("123");
        paymentRequest.setToken(new Token());
        payment.setPaymentRequest(paymentRequest);
        payment.setPaymentID("111");
        payment.setCustomerID(customer.getId());

    }

    @When("the customer requests a report")
    public void theCustomerRequestsAReport() {
        new Thread(() -> {
            try {
                Report report = customerService.getReport(customer.getId());
                reportGenerated.complete(report);
            } catch (Exception e) {
                exceptionThrown.complete(e.getMessage());
            }
        }).start();
    }

    @Then("the customer should have a report")
    public void theCustomerShouldHaveAReport() {
        Report report = reportGenerated.join();
        Assertions.assertEquals(1, report.getPayments().size());
        Assertions.assertEquals(payment, report.getPayments().get(0));
    }


    @Then("an exception with message {string} should be thrown")
    public void anExceptionWithMessageShouldBeThrown(String exceptionMessage) {
        String message = exceptionThrown.join();
        Assertions.assertEquals(exceptionMessage, message);
    }

    @Given("there is a customer not registered in DTUPay")
    public void thereIsACustomerNotRegisteredInDTUPay() {
        customer = new Client();
        customer.setName("James");
        customer.setCpr("1234567890");
        customer.setAccountId("1234567890");
        customer.setId("000");
        publishedEvents.put(customer.getId(), new CompletableFuture<Event>());
    }


}
