package dtuPay.facade;

import dtuPay.facade.adapter.rest.CustomersServiceException;
import dtuPay.facade.model.*;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ConcurrentHashMap;

// @author Ion s204710
public class CustomerFacade {

    MessageQueue mq;

    Map<CorrelationId, CompletableFuture<Client>> customerCorrelations = new ConcurrentHashMap<>();
    Map<CorrelationId, CompletableFuture<String>> customerDereigstrationCorrelations = new ConcurrentHashMap<>();
    Map<CorrelationId, CompletableFuture<ArrayList<Token>>> tokenCorrelations = new ConcurrentHashMap<>();
    Map<CorrelationId, CompletableFuture<Report>> reportCorrelations = new ConcurrentHashMap<>();

    public CustomerFacade(MessageQueue mq) {
        this.mq = mq;
        mq.addHandler("CustomerRegistered", this::handleCustomerRegistered);
        mq.addHandler("CustomerDeregistered", this::handleCustomerDeRegistered);
        mq.addHandler("TokensGenerated", this::handleTokensGenerated);
        mq.addHandler("CustomerReportGenerated", this::handleCustomerReportGenerated);
        mq.addHandler("CustomerReportNotGenerated", this::handleCustomerReportNotGenerated);
        mq.addHandler("TokensGenerationFailed", this::handleTokenGenerationFailed);
    }

    public void handleTokenGenerationFailed(Event event) {
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        String exception = event.getArgument(0, String.class);
        tokenCorrelations.get(correlationId).completeExceptionally(new CompletionException(exception, new Exception(exception)));
    }

    public void handleCustomerReportNotGenerated(Event event) {
        String exception = event.getArgument(0, String.class);
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        reportCorrelations.get(correlationId).completeExceptionally(new CompletionException(exception, new Exception(exception)));
    }

    public void handleCustomerReportGenerated(Event event) {
        Report report = event.getArgument(0, Report.class);
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        reportCorrelations.get(correlationId).complete(report);
    }

    public void handleTokensGenerated(Event event) {
        ArrayList<Token> tokens = event.getArgument(0, ArrayList.class);
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        tokenCorrelations.get(correlationId).complete(tokens);
    }

    public void handleCustomerDeRegistered(Event event) {
        String customerId = event.getArgument(0, String.class);
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        customerDereigstrationCorrelations.get(correlationId).complete(customerId);
    }

    public void handleCustomerRegistered(Event event) {
        Client customer = event.getArgument(0, Client.class);
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        customerCorrelations.get(correlationId).complete(customer);
    }

    public String register(Client customer) {
        CorrelationId correlationId = CorrelationId.randomId();
        customerCorrelations.put(correlationId, new CompletableFuture<>());
        mq.publish(new Event("CustomerRegistrationRequested", new Object[]{customer, correlationId}));
        return customerCorrelations.get(correlationId).join().getId();
    }

    public void deregister(String clientID) {
        CorrelationId correlationId = CorrelationId.randomId();
        customerDereigstrationCorrelations.put(correlationId, new CompletableFuture<>());
        mq.publish(new Event("CustomerDeregistrationRequested", new Object[]{clientID, correlationId}));
        customerDereigstrationCorrelations.get(correlationId).join();
    }

    public ArrayList<Token> getTokens(TokenRequest tokenRequest) throws Exception {
        CorrelationId correlationId = CorrelationId.randomId();
        tokenCorrelations.put(correlationId, new CompletableFuture<>());
        mq.publish(new Event("TokensGenerationRequested", new Object[]{tokenRequest, correlationId}));
        try {
            return tokenCorrelations.get(correlationId).join();
        } catch (Exception e) {
            throw new CustomersServiceException(e.getMessage());
        }
    }

    public Report getReport(String clientId) throws CustomersServiceException {
        CorrelationId correlationId = CorrelationId.randomId();
        reportCorrelations.put(correlationId, new CompletableFuture<>());
        ReportRequest reportRequest = new ReportRequest(clientId);
        mq.publish(new Event("CustomerReportRequested", new Object[]{reportRequest, correlationId}));
        try {
            return reportCorrelations.get(correlationId).join();
        } catch (Exception e) {
            throw new CustomersServiceException(e.getMessage());
        }
    }
}
