package dtuPay.facade.adapter.rest;

import dtuPay.facade.MerchantFacade;
import dtuPay.facade.model.*;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import messaging.MessageQueue;
import messaging.implementations.RabbitMqQueue;

// @author Cesare s232435
@Path("/merchants")
public class MerchantsResource {

    MerchantFacade merchantFacade = new MerchantFacade(new RabbitMqQueue("rabbitMq"));

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String register(Client merchant){
        return merchantFacade.register(merchant);
    }

    @DELETE
    @Path("/{merchantId}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deregister(@PathParam("merchantId") String merchantId) {
        merchantFacade.deregister(merchantId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/payment")
    public Transaction pay(PaymentRequest paymentRequest) throws MerchantsServiceException {
        return merchantFacade.pay(paymentRequest);
    }

    @GET
    @Path("/reports/{merchantId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Report getReports(@PathParam("merchantId") String merchantId) throws  MerchantsServiceException {
        return merchantFacade.getReport(merchantId);
    }
}