package dtuPay.facade.adapter.rest;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

// @author Matteo s222952
@Provider
public class CustomersServiceExceptionMapper implements ExceptionMapper<CustomersServiceException> {

   @Override
   public Response toResponse(CustomersServiceException exception) {
       return Response.status(Response.Status.BAD_REQUEST)
                     .entity(exception.getMessage())
                     .build();
   }
}
