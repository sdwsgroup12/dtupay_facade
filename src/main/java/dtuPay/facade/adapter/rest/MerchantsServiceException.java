package dtuPay.facade.adapter.rest;

// @author Can s232851
public class MerchantsServiceException extends Exception {

        public MerchantsServiceException() {}

        public MerchantsServiceException(String string) {
            super(string);
        }

        private static final long serialVersionUID = 1L;
}
