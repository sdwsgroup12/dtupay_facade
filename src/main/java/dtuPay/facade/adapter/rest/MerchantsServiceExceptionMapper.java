package dtuPay.facade.adapter.rest;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

// @author Ion s204710
@Provider
public class MerchantsServiceExceptionMapper implements ExceptionMapper<MerchantsServiceException> {

   @Override
   public Response toResponse(MerchantsServiceException exception) {
       return Response.status(Response.Status.BAD_REQUEST)
                     .entity(exception.getMessage())
                     .build();
   }
}
