package dtuPay.facade.adapter.rest;

// @author Abi s205720
public class CustomersServiceException extends Exception {

        public CustomersServiceException() {}

        public CustomersServiceException(String string) {
            super(string);
        }

        private static final long serialVersionUID = 1L;
}
