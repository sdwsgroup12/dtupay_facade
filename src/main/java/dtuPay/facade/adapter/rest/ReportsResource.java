package dtuPay.facade.adapter.rest;

import dtuPay.facade.ManagerFacade;
import dtuPay.facade.model.Report;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import messaging.implementations.RabbitMqQueue;

// @author Abi s205720
@Path("/reports")
public class ReportsResource {

    ManagerFacade managerFacade = new ManagerFacade(new RabbitMqQueue("rabbitMq"));

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Report getReport() {
        return managerFacade.getReport();
    }
}
