package dtuPay.facade.adapter.rest;


import dtuPay.facade.CustomerFacade;
import dtuPay.facade.model.Client;
import dtuPay.facade.model.Report;
import dtuPay.facade.model.Token;
import dtuPay.facade.model.TokenRequest;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import messaging.implementations.RabbitMqQueue;

import java.util.ArrayList;

// @author Alexander s204092
@Path("/customers")
public class CustomersResource extends Throwable {
    CustomerFacade customerFacade = new CustomerFacade(new RabbitMqQueue("rabbitMq"));

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String register(Client customer)  {
        return customerFacade.register(customer);
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{customerId}")
    public void deregister(@PathParam("customerId") String customerId) {
        customerFacade.deregister(customerId);
    }

    @POST
    @Path("/tokens")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ArrayList<Token> getTokens(TokenRequest tokenRequest) throws Exception {
        return customerFacade.getTokens(tokenRequest);
    }

    @GET
    @Path("/reports/{customerId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Report getReport(@PathParam("customerId") String customerId) throws CustomersServiceException {
        return customerFacade.getReport(customerId);
    }
}
