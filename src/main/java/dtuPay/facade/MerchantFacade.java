package dtuPay.facade;

import dtuPay.facade.adapter.rest.MerchantsServiceException;
import dtuPay.facade.model.*;
import messaging.Event;
import messaging.MessageQueue;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ConcurrentHashMap;

// @author Matteo s222952
public class MerchantFacade {

    MessageQueue mq;

    private Map<CorrelationId, CompletableFuture<Client>> merchantCorrelations = new ConcurrentHashMap<>();
    private Map<CorrelationId, CompletableFuture<String>> merchantDereigstrationCorrelations = new ConcurrentHashMap<>();
    private Map<CorrelationId, CompletableFuture<Transaction>> transactionsCorrelations = new ConcurrentHashMap<>();
    private Map<CorrelationId, CompletableFuture<Report>> reportCorrelations = new ConcurrentHashMap<>();

    public MerchantFacade(MessageQueue mq) {
        this.mq = mq;
        mq.addHandler("MerchantRegistered", this::handleMerchantRegistered);
        mq.addHandler("MerchantDeregistered", this::handleMerchantDeRegistered);
        mq.addHandler("TokenNotValidated", this::handleTokenNotValidated);
        mq.addHandler("TransactionCompleted", this::handleTransactionCompleted);
        mq.addHandler("MerchantReportGenerated", this::handleMerchantReportGenerated);
        mq.addHandler("CustomerAndMerchantBankNumbersNotRetrieved", this::handleCustomerAndMerchantBankNumbersNotRetrieved);
        mq.addHandler("PaymentServiceExceptionThrown", this::handlePaymentServiceExceptionThrown);
        mq.addHandler("MerchantReportNotGenerated", this::handleMerchantReportNotGenerated);
    }

    private void handleMerchantReportNotGenerated(Event event) {
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        String exception = event.getArgument(0, String.class);
        reportCorrelations.get(correlationId).completeExceptionally(new CompletionException(exception, new Exception(exception)));
    }

    private void handlePaymentServiceExceptionThrown(Event event) {
        String errorMessage = event.getArgument(0, String.class);
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        transactionsCorrelations.get(correlationId).completeExceptionally(new CompletionException(errorMessage, new Exception(errorMessage)));
    }

    public void handleCustomerAndMerchantBankNumbersNotRetrieved(Event event) {
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        String exception = event.getArgument(0, String.class);
        transactionsCorrelations.get(correlationId).completeExceptionally(new CompletionException(exception, new Exception(exception)));
    }

    public void handleMerchantReportGenerated(Event event) {
        Report report = event.getArgument(0, Report.class);
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        reportCorrelations.get(correlationId).complete(report);
    }

    public void handleTransactionCompleted(Event event) {
        Transaction transaction = event.getArgument(0, Transaction.class);
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        transactionsCorrelations.get(correlationId).complete(transaction);
    }

    public void handleMerchantDeRegistered(Event event) {
        String merchantId = event.getArgument(0, String.class);
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        merchantDereigstrationCorrelations.get(correlationId).complete(merchantId);
    }

    public void handleMerchantRegistered(Event event) {
        Client merchant = event.getArgument(0, Client.class);
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        merchantCorrelations.get(correlationId).complete(merchant);
    }

    public void handleTokenNotValidated(Event event) {
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        String exception = event.getArgument(0, String.class);
        transactionsCorrelations.get(correlationId).completeExceptionally(new CompletionException(exception, new Exception(exception)));
    }

    public String register(Client customer) {
        CorrelationId correlationId = CorrelationId.randomId();
        merchantCorrelations.put(correlationId, new CompletableFuture<>());
        mq.publish(new Event("MerchantRegistrationRequested", new Object[]{customer, correlationId}));
        return merchantCorrelations.get(correlationId).join().getId();
    }

    public void deregister(String clientID) {
        CorrelationId correlationId = CorrelationId.randomId();
        merchantDereigstrationCorrelations.put(correlationId, new CompletableFuture<>());
        mq.publish(new Event("MerchantDeregistrationRequested", new Object[]{clientID, correlationId}));
        merchantDereigstrationCorrelations.get(correlationId).join();
    }

    public Transaction pay(PaymentRequest paymentRequest) throws MerchantsServiceException {
        CorrelationId correlationId = CorrelationId.randomId();
        transactionsCorrelations.put(correlationId, new CompletableFuture<>());
        mq.publish(new Event("PaymentRequested", new Object[]{paymentRequest, correlationId}));
        try {
            return transactionsCorrelations.get(correlationId).join();
        } catch (Exception e) {
            throw new MerchantsServiceException(e.getMessage());
        }
    }

    public Report getReport(String clientID) throws MerchantsServiceException {
        CorrelationId correlationId = CorrelationId.randomId();
        ReportRequest reportRequest = new ReportRequest(clientID);
        reportCorrelations.put(correlationId, new CompletableFuture<>());
        mq.publish(new Event("MerchantReportRequested", new Object[]{reportRequest, correlationId}));
        try {
            return reportCorrelations.get(correlationId).join();
        } catch (Exception e) {
            throw new MerchantsServiceException(e.getMessage());
        }
    }
}
