package dtuPay.facade.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class Client implements Serializable {
    private static final long serialVersionUID = 8356741557606025130L;

    private String accountID;
    private String id;
    private String name;
    private String cpr;


    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountID;
    }

    public void setAccountId(String accountID) {
        this.accountID = accountID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpr() {
        return cpr;
    }

    public void setCpr(String cpr) {
        this.cpr = cpr;
    }

    public void generateId() {
        this.id = UUID.randomUUID().toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(accountID, client.accountID) && Objects.equals(id, client.id) && Objects.equals(name, client.name) && Objects.equals(cpr, client.cpr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountID, id, name, cpr);
    }
}
