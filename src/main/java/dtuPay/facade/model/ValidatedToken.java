package dtuPay.facade.model;

import java.io.Serializable;
import java.util.Objects;

public class ValidatedToken implements Serializable {
    private static final long serialVersionUID = 817992738204449311L;
    private String customerID;

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidatedToken that = (ValidatedToken) o;
        return Objects.equals(customerID, that.customerID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerID);
    }
}
