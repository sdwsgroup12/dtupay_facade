package dtuPay.facade.model;

import com.google.common.base.Objects;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Report implements Serializable {
    private static final long serialVersionUID = 6623738603520418005L;

    private ReportRequest reportRequest;

    private List<Payment> payments;

    public Report() {
    }

    public Report(ReportRequest reportRequest) {
        this.reportRequest = reportRequest;
    }

    public Report(List<Payment> payments) {
        this.payments = payments;
    }

    public Report(ReportRequest reportRequest, List<Payment> payments) {
        this.reportRequest = reportRequest;
        this.payments = payments;
    }

    public ReportRequest getReportRequest() {
        return this.reportRequest;
    }

    public void setReportRequest(ReportRequest reportRequest) {
        this.reportRequest = reportRequest;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Report report = (Report) o;
        return Objects.equal(reportRequest, report.reportRequest) && Objects.equal(payments, report.payments);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(reportRequest, payments);
    }

    @Override
    public String toString() {
        return "Report{" +
                "reportRequest=" + reportRequest +
                ", payments=" + payments +
                '}';
    }
}
