package dtuPay.facade.model;

import java.io.Serializable;
import java.util.Objects;

public class TokenRequest implements Serializable {
    private static final long serialVersionUID = 7784628512271660693L;
    private String customerId;
    private int amount;


    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TokenRequest that = (TokenRequest) o;
        return amount == that.amount && Objects.equals(customerId, that.customerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerId, amount);
    }
}
