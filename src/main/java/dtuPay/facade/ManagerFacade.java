package dtuPay.facade;

import dtuPay.facade.model.CorrelationId;
import dtuPay.facade.model.Report;
import messaging.Event;
import messaging.MessageQueue;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

// @author Abi s205720
public class ManagerFacade {
    MessageQueue mq;
    Map<CorrelationId, CompletableFuture<Report>> reportCorrelations = new ConcurrentHashMap<>();

    public ManagerFacade(MessageQueue mq) {
        this.mq = mq;

        mq.addHandler("ManagerReportGenerated", this::handleManagerReportGenerated);
    }

    public void handleManagerReportGenerated(Event event) {
        Report report = event.getArgument(0, Report.class);
        CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
        reportCorrelations.get(correlationId).complete(report);
    }

    public Report getReport() {
        CorrelationId correlationId = CorrelationId.randomId();
        reportCorrelations.put(correlationId, new CompletableFuture<>());
        mq.publish(new Event("ManagerReportRequested", new Object[]{correlationId}));
        return reportCorrelations.get(correlationId).join();
    }
}
